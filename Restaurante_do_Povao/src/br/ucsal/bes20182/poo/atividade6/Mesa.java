package br.ucsal.bes20182.poo.atividade6;

public class Mesa {
	private Integer numeracao;
	private Integer quantidadepessoas;
	private SituacaoMesaEnum situacao;



	public Mesa() {
		super();
	}
	public Mesa(Integer numeracao, Integer quantidadepessoas,SituacaoMesaEnum situacao) {
		super();
		this.numeracao = numeracao;
		this.quantidadepessoas = quantidadepessoas;
		this.setSituacao(situacao);
	}
	public Integer getNumeracao() {
		return numeracao;
	}
	public void setNumeracao(Integer numeracao) {
		this.numeracao = numeracao;
	}
	public Integer getQuantidadepessoas() {
		return quantidadepessoas;
	}
	public void setQuantidadepessoas(Integer quantidadepessoas) {
		this.quantidadepessoas = quantidadepessoas;
	}
	public SituacaoMesaEnum getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
	}




}
